﻿using OSGeo.GDAL;
using OSGeo.OGR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace Estadisticas
{
    class Program
    {
        static void Main(string[] args)
        {

            Dataset dataset = Gdal.Open(@"C:\Users\j.alfaro\OneDrive - PROTECCION AGROPECUARIA COMPAÑIA DE SEGUROS, S.A\Imágenes\tiff\responseanalytics.tif",Access.GA_ReadOnly);

            // Get the GDAL Band objects from the Dataset
            //Prueba seria
            Band band = dataset.GetRasterBand(1);
            ColorTable ct = band.GetRasterColorTable();
            // Create a Bitmap to store the GDAL image in
            Bitmap bitmap = new Bitmap(128, 128, PixelFormat.Format8bppIndexed);
            // Obtaining the bitmap buffer
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, 128, 128), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
            try
            {
                int iCol = ct.GetCount();
                ColorPalette pal = bitmap.Palette;
                for (int i = 0; i < iCol; i++)
                {
                    ColorEntry ce = ct.GetColorEntry(i);
                    pal.Entries[i] = Color.FromArgb(ce.c4, ce.c1, ce.c2, ce.c3);
                }
                bitmap.Palette = pal;

                int stride = bitmapData.Stride;
                IntPtr buf = bitmapData.Scan0;

                band.ReadRaster(0, 0, 128, 128, buf, 128, 128, DataType.GDT_Byte, 1, stride);
            }
            finally
            {
            //Final parte 2 ...
                bitmap.UnlockBits(bitmapData);
            }


            //Gdal.AllRegister();
            //Ogr.RegisterAll();

            //Dataset dataset = Gdal.Open(@"C:\Users\j.alfaro\OneDrive - PROTECCION AGROPECUARIA COMPAÑIA DE SEGUROS, S.A\Imágenes\tiff\responseanalytics.tif", Access.GA_ReadOnly);
            //Band band = dataset.GetRasterBand(1);
            //int width = band.XSize;
            //int height = band.YSize;
            //int size = width * height;
            //double min = 0.00;
            //double max = 0.00;
            //double mean = 0.00;
            //double stddev = 0.00;
            //double noData = 0.00;
            //int hasNodata = 0;

            //band.GetNoDataValue(out noData, out hasNodata);

            //var stats = band.GetStatistics(1, 0, out min, out max, out mean, out stddev);

            ////Console.WriteLine($"Statistics retrieved and returned a result of {stats}");
            //Console.WriteLine($"X : {width} Y : {height} SIZE: {size}");
            //Console.WriteLine($"MIN : {min} MAX : {max} MEAN : {mean} STDDEV : {stddev}");
            //Console.WriteLine($"HAS NO DATA : {hasNodata} Value : {noData}");
            //DataType type = band.DataType;
            //Console.WriteLine($"Data Type : {type}");


            //double gtMean = 0; //cut
            //double ltMean = 0; //fill
            //int noDataCount = 0;

            //float[] data = new float[size];

            //var dataArr = band.ReadRaster(0, 0, width, height, data, width, height, 0, 0);

            //float fMean = (float)mean;
            //float fNoData = (float)noData;
            //int i, j;
            //for (i = 0; i < width; i++)
            //{
            //    for (j = 0; j < height; j++)
            //    {
            //        float value = data[i + j * width];
            //        if (value != fNoData)
            //        {
            //            if (value > fMean)
            //            {
            //                gtMean += value - fMean;
            //            }

            //            else if (value < fMean)
            //            {
            //                ltMean += fMean - value;
            //            }
            //        }
            //        else
            //        {
            //            noDataCount++;
            //        }

            //    }
            //}

            //Console.WriteLine($"Sum of values above the mean {gtMean}");
            //Console.WriteLine($"Sum of values below the mean {ltMean}");
            //Console.WriteLine($"Cells W/ Data : {size - noDataCount} Cells W/ No Data : {noDataCount}");

            //double pixelArea = 0.10763911106; //cubic feet raster is (10cm x 10cm pixel)
            //double dLtMean = (double)ltMean;
            //double dGtMean = (double)gtMean;

            //double ltMeanFt3 = dLtMean * pixelArea; //fill in cubic feet
            //double gtMeanFt3 = dGtMean * pixelArea; //cut in cubic feet

            //Console.WriteLine($"Cut : {gtMeanFt3} cubic feet");
            //Console.WriteLine($"Fill : {ltMeanFt3} cubic feet");

            //var meanDelta = gtMeanFt3 - ltMeanFt3;

            //if (meanDelta > 0)
            //{
            //    Console.WriteLine($"Excess Cut: {meanDelta} cubic feet");
            //}
            //else if (meanDelta < 0)
            //{
            //    Console.WriteLine($"Insufficient Fill: {meanDelta} cubic feet");
            //}
            //else
            //{
            //    Console.WriteLine("cut & fill are balanced");
            //}

            //Console.WriteLine("Press Any Key To Exit....");
            //Console.ReadLine();
        }
    }
}