﻿using OSGeo.GDAL;
using OSGeo.OGR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using OpenCvSharp;
using Point = OpenCvSharp.Point;
using OpenCvSharp.Extensions;

namespace Estadisticas
{
    class Program
    {
        static void Main(string[] args)
        {


            /* Gdal.AllRegister();

             Dataset dataset = Gdal.Open(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\1-QRO-1-2841\706-1_21002021110022_F769.tif", Access.GA_ReadOnly);
             Band band = dataset.GetRasterBand(1);
             int width = band.XSize;
             int height = band.YSize;
             int size = width * height;
             double min = 0.00;
             double max = 0.00;
             double mean = 0.00;
             double stddev = 0.00;
             double noData = 0.00;
             int hasNodata = 0;

             band.GetNoDataValue(out noData, out hasNodata);

             var stats = band.GetStatistics(1, 0, out min, out max, out mean, out stddev);

             Console.WriteLine($"X : {width} Y : {height} Tamaño: {size}");
             Console.WriteLine($"MÍNIMO : {min*100}");
             Console.WriteLine($"MÁXIMO : {max * 100}");
             Console.WriteLine($"PROMEDIO : {mean * 100}");
             Console.WriteLine($"STDDEV : {stddev * 100}");*/

            /*Mat image = Cv2.ImRead(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\Paleta_Prueba\prueba.bmp");*/

            /*Point[][] contours = GetAllContours(image);
            Mat imageClone = image.Clone();
            Cv2.DrawContours(imageClone, contours, -1, new Scalar(0, 0, 0), thickness: 3);
            Cv2.ImShow("Contours", imageClone);

            Cv2.ImShow("Out", image);
            Cv2.WaitKey();*/

            /*var src = new Mat(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\1-QRO-1-3016\Paleta\743-1_16392021013956_F4360_2.png", ImreadModes.Grayscale);*/
            /* var src = new Mat(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\1-CHIH-3-7114\Paleta\840-3_16002021110017_F1156.png", ImreadModes.Grayscale);*/
            //var src = Cv2.ImRead(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\1-CHIH-3-7114\Paleta\840-2_16052021100525_F43.png", ImreadModes.Color);


            /*var gray_mat = new Mat();
            Cv2.CvtColor(src, gray_mat, ColorConversionCodes.BGR2GRAY);

            var bw_mat = new Mat();
            Cv2.Threshold(gray_mat, bw_mat, 192, 255, ThresholdTypes.Trunc);

            Mat r3 = new Mat();
            OpenCvSharp.Point[][] contours;
            OpenCvSharp.HierarchyIndex[] hindex;
            Cv2.FindContours(bw_mat, out contours, out hindex, RetrievalModes.External, ContourApproximationModes.ApproxTC89L1);
            Cv2.DrawContours(src, contours, -1, new Scalar(0, 0, 255), 2, LineTypes.AntiAlias);*/

            /*var src = new Mat(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\1-CHIH-3-7114\Paleta\840-1_16052021100521_F42.png", ImreadModes.Color);*/

            /*var src = new Mat(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\1-QRO-1-3016\Paleta\743-1_16392021013956_F4360_2.png", ImreadModes.Color);*/

            var src = new Mat(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\1-QRO-1-3016\Paleta\743-1_16392021013956_F4360_2.png", ImreadModes.Color);

            /* Mat result = new Mat(src.Rows, src.Cols, MatType.CV_8UC1);*/
            Mat bin = new Mat();
            /*src.CopyTo(result);*/
            var bw_mat = new Mat();
            Cv2.CvtColor(src, bin, ColorConversionCodes.BGR2GRAY);
            Cv2.Threshold(bin, bw_mat, 192, 255, ThresholdTypes.Trunc);
            Cv2.ImShow("src", src);
            Mat r2 = new Mat();
            Cv2.FindContours(bin, out Mat[] contours, r2, RetrievalModes.External, ContourApproximationModes.ApproxTC89L1);
            /*Cv2.DrawContours(src, contours, -1, Scalar.Blue, 1, LineTypes.AntiAlias);*/
            Point[][] hull = new Point[contours.Length][];
            Mat r3 = new Mat();
            for (int i = 0; i <= contours.Length-1; i++)
            {
                Cv2.ConvexHull(contours[i], r3);
            }

            Cv2.DrawContours(src, new Mat[] { r3 }, 0, new Scalar(0, 0, 5), 1, LineTypes.AntiAlias);

            Point[][] contour;
            HierarchyIndex[] hindex;
            Cv2.FindContours(bw_mat, out contour, out hindex, RetrievalModes.External, ContourApproximationModes.ApproxTC89L1);
           /* Cv2.DrawContours(src, contours, -1, Scalar.Yellow, 5, LineTypes.AntiAlias);*/
            /*Cv2.ImShow("result", result);*/
            /*            Cv2.WaitKey(0);
                        Cv2.DestroyAllWindows();*/


            src.SaveImage(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\Paleta_Prueba\prueba21.png");

            var src2 = new Mat(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\Paleta_Prueba\prueba19.png", ImreadModes.AnyColor);
/*
            src2.SaveImage(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\Paleta_Prueba\prueba20.png");*/

            /*var gray_mat = new Mat();
            Cv2.CvtColor(src, gray_mat, ColorConversionCodes.BGR2GRAY);

            var bw_mat = new Mat();
            Cv2.Threshold(gray_mat, bw_mat, 192, 255, ThresholdTypes.Trunc);

            OpenCvSharp.Point[][] contours;
            OpenCvSharp.HierarchyIndex[] hindex;
            Cv2.FindContours(bw_mat, out contours, out hindex, RetrievalModes.External, ContourApproximationModes.ApproxTC89L1);
            Cv2.DrawContours(src, contours, -1, new Scalar(0, 0, 255), 2, LineTypes.AntiAlias);


            src.SaveImage(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\Paleta_Prueba\prueba17.png");*/

            /*Mat r2 = new Mat();
            Cv2.FindContours(src, out Mat[] contours, r2, RetrievalModes.External, ContourApproximationModes.ApproxTC89KCOS);

            Mat r3 = new Mat();
            Cv2.ApproxPolyDP(contours[0], r3, 5, true);

            Mat result = new Mat(src.Rows, src.Cols, MatType.CV_8UC3);
            Cv2.DrawContours(result, new Mat[] { r3 }, 0, new Scalar(0, 0, 255), 1, LineTypes.AntiAlias);*/

            /* Mat r2 = new Mat();
             Cv2.FindContours(src, out Mat[] contours, r2, RetrievalModes.External, ContourApproximationModes.ApproxTC89L1);

             Mat r3 = new Mat();
             Cv2.ConvexHull(contours[0], r3);

             Mat result = new Mat(src.Rows, src.Cols, MatType.CV_8UC3);
             Cv2.DrawContours(result, contours, 0, new Scalar(0, 0, 255), 1, LineTypes.AntiAlias);
             Cv2.DrawContours(result, new Mat[] { r3 }, 0, new Scalar(0, 255, 0), 1, LineTypes.AntiAlias);*/


            /*var src = new Mat(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\1-QRO-1-3016\Paleta\743-1_16392021013956_F4360_2.png", ImreadModes.Color);*/

        }

       /* static Point[][] GetAllContours(Mat img)
        {
            Mat refGray = new Mat();
            Cv2.CvtColor(img, refGray, ColorConversionCodes.BGR2GRAY);
            Mat thresh = new Mat();
            Cv2.Threshold(refGray, thresh, 127, 255, ThresholdTypes.Binary);

            Point[][] contours;
            HierarchyIndex[] hIndx;
            Cv2.FindContours(thresh, out contours, out hIndx, RetrievalModes.List, ContourApproximationModes.ApproxSimple);

            return contours;
        }*/

        /*   using (var src = new Mat(@"C:\Proagro\Insurtech\panel\agromonitor\agromaps\geotech\FCover\Interpolada\Paleta_Prueba\prueba.bmp"))
           using (var gray = new Mat())
           {
               using (var bw = src.CvtColor(ColorConversionCodes.BGR2GRAY)) // convert to grayscale
               {
                   // invert b&w (specific to your white on black image)
                   Cv2.BitwiseNot(bw, gray);
               }

               // find all contours
               var contours = gray.FindContoursAsArray(RetrievalModes.List, ContourApproximationModes.ApproxSimple);
               using (var dst = src.Clone())
               {
                   foreach (var contour in contours)
                   {
                       // filter small contours by their area
                       var area = Cv2.ContourArea(contour);
                       if (area < 15 * 15) // a rect of 15x15, or whatever you see fit
                           continue;

                       // also filter the whole image contour (by 1% close to the real area), there may be smarter ways...
                       if (Math.Abs((area - (src.Width * src.Height)) / area) < 0.01f)
                           continue;

                       var hull = Cv2.ConvexHull(contour);
                       Cv2.Polylines(dst, new[] { hull }, true, Scalar.Red, 2);
                   }

                   using (new Window("src image", src))
                   using (new Window("dst image", dst))
                   {
                       Cv2.WaitKey();
                   }
               }
           }*/
    

            /* DataType type = band.DataType;
             Console.WriteLine($"Data Type : {type}");


             double gtMean = 0; //cut
             double ltMean = 0; //fill
             int noDataCount = 0;

             float[] data = new float[size];

             var dataArr = band.ReadRaster(0, 0, width, height, data, width, height, 0, 0);

             float fMean = (float)mean;
             float fNoData = (float)noData;
             int i, j;
             for (i = 0; i < width; i++)
             {
                 for (j = 0; j < height; j++)
                 {
                     float value = data[i + j * width];
                     if (value != fNoData)
                     {
                         if (value > fMean)
                         {
                             gtMean += value - fMean;
                         }

                         else if (value < fMean)
                         {
                             ltMean += fMean - value;
                         }
                     }
                     else
                     {
                         noDataCount++;
                     }

                 }
             }

             Console.WriteLine($"Sum of values above the mean {gtMean}");
             Console.WriteLine($"Sum of values below the mean {ltMean}");
             Console.WriteLine($"Cells W/ Data : {size - noDataCount} Cells W/ No Data : {noDataCount}");

             double pixelArea = 0.10763911106; //cubic feet raster is (10cm x 10cm pixel)
             double dLtMean = (double)ltMean;
             double dGtMean = (double)gtMean;

             double ltMeanFt3 = dLtMean * pixelArea; //fill in cubic feet
             double gtMeanFt3 = dGtMean * pixelArea; //cut in cubic feet

             Console.WriteLine($"Cut : {gtMeanFt3} cubic feet");
             Console.WriteLine($"Fill : {ltMeanFt3} cubic feet");

             var meanDelta = gtMeanFt3 - ltMeanFt3;

             if (meanDelta > 0)
             {
                 Console.WriteLine($"Excess Cut: {meanDelta} cubic feet");
             }
             else if (meanDelta < 0)
             {
                 Console.WriteLine($"Insufficient Fill: {meanDelta} cubic feet");
             }
             else
             {
                 Console.WriteLine("cut & fill are balanced");
             }*/

            /*     Console.WriteLine("Press Any Key To Exit....");
                 Console.ReadLine();
             }*/
        }
}